//Adiciona um ouvinte de events a um elemento HTML

const formulario = document.getElementById("formulario1")

formulario.addEventListener("submit", function (evento)
{
    evento.preventDefault();/*previne o comportamento padrão e um elemento HTML em resposta a um evento*/

    //variaveis para tratar os dados recebidos dos elementos do formulario
    const nome = document.getElementById("nome").value;
    const email =document.getElementById("email").value;

    //Exibe um alerta com os dados coletados
    alert(`Nome: ${nome} --- E mail: ${email}`);
});






